variable "lambda_function_name" {
  type = string
}

variable "s3_bucket_name" {
  type = string
}

variable "sqs_name" {
  type = string
}

variable "aws_iam_policy_name" {
  type = string
}

variable "aws_iam_role_name" {
  type = string
}

variable "state-file-bucket-name" {
  type = string
}
