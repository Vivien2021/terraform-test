data "archive_file" "lambda_function" {
  type        = "zip"
  source_file = "${path.module}/lambda_function.js"
  output_path = "${path.module}/lambda_function.zip"
}

resource "aws_lambda_function" "email_service_lambda" {
  function_name = "${var.lambda_function_name}"
  filename = "${data.archive_file.lambda_function.output_path}"
  description = "send email to user"
  handler = "lambda_function.handler"
  runtime = "nodejs14.x"
  role = "${aws_iam_role.email_service_role.arn}"
}

resource "aws_lambda_event_source_mapping" "trigger" {
  event_source_arn = "${aws_sqs_queue.email_service_sqs.arn}"
  function_name = "${aws_lambda_function.email_service_lambda.arn}"
  batch_size = 1
  enabled = true
}

