provider "aws" {
  region  = "ap-southeast-2"
  profile = "vivien-admin"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "my-vpc"
  cidr = "12.0.0.0/16"

  azs             = ["ap-southeast-2a", "ap-southeast-2a", "ap-southeast-2a"]
  private_subnets = ["12.0.1.0/24", "12.0.2.0/24", "12.0.3.0/24"]
  public_subnets  = ["12.0.101.0/24", "12.0.102.0/24", "12.0.103.0/24"]

  enable_nat_gateway = true

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}
