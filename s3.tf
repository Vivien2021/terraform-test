resource "aws_s3_bucket" "email_service_s3bucket" {
    bucket = "${var.s3_bucket_name}"
    acl = "public-read"  
}