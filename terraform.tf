terraform {
  backend "s3" {
    encrypt              = true
    bucket               = "terraform-serverless-jrdevops-vivien"
    region               = "ap-southeast-2"
    key                  = "test.tfstate"
    profile              = "vivien-admin"
    workspace_key_prefix = "env:"
    # dynamodb_table = "terraform-state-lock-dynamodb"
  }
}
