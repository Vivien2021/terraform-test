output "sqs_url" {
  value = "${aws_sqs_queue.email_service_sqs.id}"
}